import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { HeaderComponent } from './components/header/header.component';

export const SHARED_DIRECTIVES = [
  // SomeDirective
];
export const SHARED_COMPONENTS = [
  // SomeComponent
  HeaderComponent
];
export const SHARED_PROVIDERS = [
  // SomeService
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule,
  ],
  declarations: [
    ...SHARED_DIRECTIVES,
    ...SHARED_COMPONENTS
  ],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule,
    ...SHARED_DIRECTIVES,
    ...SHARED_COMPONENTS
  ]
})
export class SharedModule { }
