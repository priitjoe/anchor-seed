import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared.module';

import { HomeComponent } from './home.component';
import { ROUTES } from './home.routes';

export const HOME_COMPONENTS = [HomeComponent];

@NgModule({
  imports: [
    SharedModule,
    ROUTES
  ],
  declarations: [
    ...HOME_COMPONENTS
  ],
  providers: [
    // AuthGuard,
    // HomeService,
    // HomeGuard
  ],
  exports: [
    ...HOME_COMPONENTS
  ]
})
export class HomeModule {

}
