import { Component } from '@angular/core';

@Component({
  selector: 'anchor-app',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor() {
    // ...
  }
}
